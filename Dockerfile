FROM smebberson/alpine-nginx-nodejs

# RUN apk add --no-cache make bash
# # RUN apk add --no-cache make bash git python alpine-sdk g++

MAINTAINER Cristobal Gomez <me@cristobal.io>

RUN mkdir -p /app
WORKDIR /app

ENV NODE_ENV production
ENV AVIATION_API_HOSTNAME aviation-api.cristobal.io
ENV AVIATION_API_PORT 443
ENV AVIATION_CORS_ORIGIN http://aviation-api.cristobal.io

COPY . /app

RUN npm install && \
  npm run clean-dist && \
  NODE_ENV=production npm run dist && \
  cp -R ./dist/* /usr/html/ && \
  npm cache clean && \
  rm -rf /app/

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log