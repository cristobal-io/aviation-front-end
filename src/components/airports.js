import React from "react";
import { connect } from "react-redux";
import { LayerGroup } from "react-leaflet";

import Airport from "./airport";


class airports extends React.Component {
  render(){
    const { airports, overlay } = this.props;

    const viewAirports = overlay.indexOf("Airports") !== -1;

    return (
      <LayerGroup name="airports2">
      {viewAirports? airports.map(airport=> {
        if (airport.airlines_flying !== 0) {
        return (
          <Airport key={airport.airport_id} airport={airport} />
          );
        }
        }) : null
      }
      </LayerGroup>
      );
    
  }
}


airports.propTypes = {
  airports: React.PropTypes.array,
  overlay: React.PropTypes.array

};

const mapStateToProps = state => {
  return {
    airports: state.airports,
    overlay: state.overlay
  };
};

const airportsConnected = connect(mapStateToProps)(airports);

export default airportsConnected;