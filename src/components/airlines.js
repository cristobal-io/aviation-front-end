import React from "react";
import { connect } from "react-redux";
import Airline from "./airline.js";


class airlines extends React.Component {
  render(){
    const { airlines } = this.props;
    const airlinesKeys = Object.keys(airlines);
    return (
      <div className="table-responsive">
        
      {airlinesKeys.length? <h2 className="text-center">Airlines:</h2> : null}
        <table className="table table-hover table-sm">
          <tbody>
              {airlinesKeys.map((airline)=>{
                return (
                  <Airline key={airline}>{airline}</Airline>
                  );
              })}
          </tbody>
        </table>
      </div>
      );
  }
}


airlines.propTypes = {
  airlines: React.PropTypes.object
};

const mapStateToProps = state => {
  return {
    airlines: state.airlines,
  };
};


const airlinesConnected = connect(mapStateToProps)(airlines);

export default airlinesConnected;