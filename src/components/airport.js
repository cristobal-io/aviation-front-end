import React from "react";
import { connect } from "react-redux";
import { Marker } from "react-leaflet";
import L from "leaflet";

import {
  changeSidebarVisibility,
  getRunways,
  getAirlines,
  updateFocusedAirport
} from "../actions/actions";

import { viewDestinationsInfo } from "../data/notifications";

// notif-redux module.
import { actions as notifActions } from "redux-notifications";
const { notifSend } = notifActions;

class airport extends React.Component {
  render(){
    const { toogleView, focusedAirport, sidebarVisible, toogleSidebar, airport, dispatchNotification } = this.props;
    
    let active = false;
    if (focusedAirport !== undefined && airport !== undefined) {
      active = focusedAirport.airport_id === airport.airport_id;
    }

    // we need to require the images, so webpack uses them when dist folder
    // is generated.
    const airplaneUrlBlue = require("../styles/images/aircraftsmall.png");
    const airplaneUrlRed = require("../styles/images/aircraftsmall_red.png");
    const airportUrlBlue = require("../styles/images/airport.png");
    const airportUrlRed = require("../styles/images/airport_red.png");
    
    const airplaneIcon = L.icon({
      iconUrl: active? airplaneUrlRed : airplaneUrlBlue ,
      iconSize: [20, 24],
      iconAnchor: [15, 30],
      className: `${active? "airport-active" : "airport" }`
    });

    const airportIcon  = L.icon({
      iconUrl: active? airportUrlRed : airportUrlBlue,
      iconSize: [32, 37],
      iconAnchor: [15, 30],
      className: `${active? "airport-active" : "airport" }`
    });



    return (
        <Marker 
          id={airport.airport_id}
          key={airport.airport_id} 
          position={[airport.dd_latitude, airport.dd_longitude]}
          icon={airport.airlines_flying !== 0 ? airportIcon : airplaneIcon}
          title={airport.name}
          onClick={()=>{
            toogleView(airport);
            if (
              (airport.airport_id === focusedAirport.airport_id || focusedAirport.airport_id === undefined) || 
              (airport.airport_id !== focusedAirport.airport_id && !sidebarVisible)
              ) {
              toogleSidebar(airport);
            }
            if (!sidebarVisible) {
              dispatchNotification(viewDestinationsInfo);
            }
          }} />
        );
  }
}


airport.propTypes = {
  airport: React.PropTypes.object,
  toogleView: React.PropTypes.func,
  focusedAirport: React.PropTypes.object,
  toogleSidebar: React.PropTypes.func,
  sidebarVisible: React.PropTypes.bool,
  dispatchNotification: React.PropTypes.func
};

const mapStateToProps = state => {
  return {
    focusedAirport: state.focusedAirport,
    sidebarVisible: state.sidebarVisible
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toogleView: (airport) => {
      dispatch(updateFocusedAirport(airport));
      dispatch(getRunways(airport.name));
      dispatch(getAirlines(airport.airport_id));
    },
    toogleSidebar: () => dispatch(changeSidebarVisibility()),
    dispatchNotification: options => dispatch(notifSend(options))
  };
};

const airportConnected = connect(mapStateToProps, mapDispatchToProps)(airport);

export default airportConnected;