import React from "react";
import { connect } from "react-redux";
import { updateNotificationClosed } from "../actions/actions";
import { actions as notifActions } from "redux-notifications";
const { notifDismiss } = notifActions;

class topBox extends React.Component {

  render (){
    const { kind, componentClassName, message, id, clearSingleNotif, notificationsClosed} = this.props;

    if (notificationsClosed === undefined || notificationsClosed.indexOf(id) === -1) {

      return (
        <div className={`${componentClassName} ${componentClassName}--${kind}`}>
          <div className={`${componentClassName}__icon`} />
          <div className={`${componentClassName}__content`}>
            <span className={`${componentClassName}__message`}>{message}</span>
          </div>
          <span className={`${componentClassName}__action`}>
            <button onClick={()=>clearSingleNotif(id)}>Close</button>
          </span>
        </div>
        );
    } else {
      return (<div />);
      
    }
  }
}

topBox.propTypes = {
  message: React.PropTypes.string.isRequired,
  kind: React.PropTypes.oneOf([
    "success",
    "info",
    "warning",
    "danger"
  ]).isRequired,
  componentClassName: React.PropTypes.string,
  id: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
    ]),
  clearSingleNotif: React.PropTypes.func,
  notificationsClosed: React.PropTypes.array
};

const mapStateToProps = state => {
  return {
    notificationsClosed: state.notificationsClosed
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    clearSingleNotif: (id) => {
      dispatch(notifDismiss(id));
      dispatch(updateNotificationClosed(id));
    }
  };
};

const topBoxConnected = connect(mapStateToProps, mapDispatchToProps)(topBox);

export default topBoxConnected;