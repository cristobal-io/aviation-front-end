import React from "react";
import { connect } from "react-redux";
import { LayerGroup } from "react-leaflet";

import Airport from "./airport";


class airfields extends React.Component {
  render(){
    const { airports, overlay } = this.props;
    const showAirfields = overlay.indexOf("Airfields") !== -1;

    return (
      <LayerGroup name="airports2">
      {showAirfields? airports.map(airport=> {
        if (airport.airlines_flying === 0) {
        return (
          <Airport key={airport.airport_id} airport={airport} />
          );
        }
        }) : null
      }
      </LayerGroup>
      );
  }
}


airfields.propTypes = {
  airports: React.PropTypes.array,
  overlay: React.PropTypes.array
};

const mapStateToProps = state => {
  return {
    airports: state.airports,
    overlay: state.overlay
  };
};

const airfieldsConnected = connect(mapStateToProps)(airfields);

export default airfieldsConnected;