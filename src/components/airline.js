import React from "react";
import { connect } from "react-redux";

import { 
  airlineIsFocused,
  getDestinations,
  setFreezeLatestZoomStatus,
  updateFocusedAirline,
  updateLatestZoom,
  updateOldDestinationsCount,
  updateOldFocusedAirline
} from "../actions/actions";
import classnames from "classnames";

// notif-redux module.
import { actions as notifActions } from "redux-notifications";
const { notifSend } = notifActions;
// notifications
import { selectOtherAirportInfo, unselectInfo, selectOtherAirlineInfo } from "../data/notifications";


class airline extends React.Component {
  componentDidUpdate(){
    this.focusAirports();

  }

  focusAirports(){
    const { 
      airlineDestinations,
      airlineFocusedStatus,
      dispatchOldDestinationsCount,
      dispatchOldFocusedAirline,
      lastDestinationsCount,
      leafletElement,
      focusedAirline,
      
    } = this.props;

    if (airlineDestinations.length > 0 && airlineFocusedStatus) {
        if (lastDestinationsCount !== airlineDestinations.length) {
          dispatchOldDestinationsCount(airlineDestinations.length);
          dispatchOldFocusedAirline(focusedAirline);
          leafletElement.fitBounds(airlineDestinations, {paddingTopLeft:[300,0]});
        }
        
      // }
    }
  }

  render(){
    const { 
      airlines,
      showAirlineAirports,
      focusedAirline,
      setOldZoom,
      leafletElement,
      toogleAirlineIsFocused,
      airlineFocusedStatus,
      oldZoom,
      focusedAirport,
      dispatchNotification,
      setFocusedAirline,
      dispatchOldDestinationsCount,
      freezeOldZoom
    } = this.props;
    let airline = this.props.children;

    const airlineClasses = classnames( {
      info: focusedAirline.airline_id === airline && airlineFocusedStatus
    });

    airline = airlines[airline];
    return (
      <tr
      className={airlineClasses} 
      onClick={()=> {
        if (!airlineFocusedStatus ) {
          // airline selected first time
          
          // showing notifications
          dispatchNotification(selectOtherAirportInfo);
          dispatchNotification(unselectInfo);
          dispatchNotification(selectOtherAirlineInfo);
          
          // updating airline destination airports
          showAirlineAirports(airline);

          setFocusedAirline(airline);

          // get an old zoom and freeze it 
          // in case we unselect we can get back to the previous zoom.
          setOldZoom(leafletElement.getZoom());
          freezeOldZoom(true);
          toogleAirlineIsFocused();
        } else if (focusedAirline.airline_id === airline.airline_id) {
          // we are unselecting the airline.
          toogleAirlineIsFocused();
          dispatchOldDestinationsCount(0);
          setFocusedAirline({});
          leafletElement.setView({lat: focusedAirport.dd_latitude, lng: focusedAirport.dd_longitude}, oldZoom);
        } else if (focusedAirline.airline_id !== airline.airline_id) {
          // we have selected a different airline
          
          freezeOldZoom(true);

          // updating airline destination airports
          showAirlineAirports(airline);
          setFocusedAirline(airline);
        }
      }}>      
        <td className="airline-container" >
          <img className="img-responsive center-block" src={"https:" + airline.logo_url} alt={airline.name}/>
          <br/>
          <p className="text-center">
            <span title="IATA Code">{airline.iata}</span> - <span title="ICAO Code">{airline.icao}</span>
          </p>
          <p className="text-center" >
            
           <a href={airline.website}>visit website</a>
          </p>
        </td>
      </tr>
      );
  }
}

airline.propTypes = {
  airlines: React.PropTypes.object,
  airlineDestinations: React.PropTypes.array,
  airlineFocusedStatus: React.PropTypes.bool,
  children: React.PropTypes.string,
  dispatchNotification: React.PropTypes.func,
  dispatchOldDestinationsCount: React.PropTypes.func,
  dispatchOldFocusedAirline:React.PropTypes.func,
  freezeOldZoom: React.PropTypes.func,
  focusedAirline: React.PropTypes.object,
  focusedAirport: React.PropTypes.object,
  leafletElement: React.PropTypes.object,
  lastDestinationsCount: React.PropTypes.number,
  oldZoom: React.PropTypes.number,
  setFocusedAirline: React.PropTypes.func,
  setOldZoom: React.PropTypes.func,
  showAirlineAirports: React.PropTypes.func,
  toogleAirlineIsFocused: React.PropTypes.func,


};

const mapStateToProps = state => {
  return {
    airlines: state.airlines,
    airlineDestinations: state.airlineDestinations,
    airlineFocusedStatus: state.airlineIsFocused,
    focusedAirline: state.focusedAirline,
    focusedAirport: state.focusedAirport,
    lastDestinationsCount: state.lastDestinationsCount,
    leafletElement: state.leafletElement,
    oldZoom: state.oldZoom
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatchNotification: options => dispatch(notifSend(options)),
    dispatchOldDestinationsCount: count => dispatch(updateOldDestinationsCount(count)),
    dispatchOldFocusedAirline: airline => dispatch(updateOldFocusedAirline(airline)),
    freezeOldZoom: freeze => dispatch(setFreezeLatestZoomStatus(freeze)),
    setFocusedAirline: airline => dispatch(updateFocusedAirline(airline)),
    setOldZoom: zoom => dispatch(updateLatestZoom(zoom)),
    showAirlineAirports: (airline) => dispatch(getDestinations(airline.airline_id)),
    toogleAirlineIsFocused: () => dispatch(airlineIsFocused()),


  };
};

const airlineConnected = connect(mapStateToProps, mapDispatchToProps)(airline);

export default airlineConnected;
