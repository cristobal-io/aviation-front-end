// import dependencies
import React from "react";
import { connect } from "react-redux";
import { Map, TileLayer , ZoomControl, LayersControl} from "react-leaflet";
// notif-redux module.
import { Notifs, actions as notifActions } from "redux-notifications";
const { notifSend, notifDismiss } = notifActions;

import { zoomWarning, initialInfo } from "../data/notifications";
// import actions
import {
  center,
  changeSidebarVisibility,
  defineLeafletElement,
  getAirports,
  setFreezeLatestZoomStatus,
  updateOverlay,
  updateLatestZoom,
  zoom
} from "../actions/actions";

// import components
import Airfields from "./airfields";
import Airports from "./airports";
import Sidebar from "./sidebar";
import TopBox from "./topBox";

  
class map extends React.Component {

  componentDidMount(){
    // we can not use the leafletElement on props because we assing it here.
    const mapElement = this.refs.map.leafletElement;
    const northEast = mapElement.getBounds().getNorthEast();
    const southWest = mapElement.getBounds().getSouthWest();
    const { dispatchCenter, dispatchGetAirports, dispatchLeafletElement, dispatchNotification} = this.props;

    // we request the browser position to center the map.
    navigator.geolocation.getCurrentPosition(function(position) {
      dispatchCenter(position.coords.latitude, position.coords.longitude);
    });

    // get the airports on the area.
    dispatchGetAirports(northEast, southWest);
    dispatchLeafletElement(mapElement);

    // show notification
    dispatchNotification(initialInfo);
  }

  componentDidUpdate(){
    this.warningZoomMessage();
  }

  warningZoomMessage (){
    const {
      clearNotif,
      dispatchNotification,
      maxZoomUpdate,
      notifs,
      zoom
    } = this.props;

    let zoomNotif = false;
    notifs.map(notif => {
      if (notif.id === "bigZoom" ) {
        zoomNotif = true;
      }
    });
    if (zoom <= maxZoomUpdate) {
      if (!zoomNotif) {
        dispatchNotification(zoomWarning);
      }
    } else {
      if (zoomNotif) {
        clearNotif("bigZoom");
      }
    }
  }
  

  render() {
    const { 
     animate,
     airlineIsFocused,
     center,
     freezeOldZoom,
     focusedAirport,
     getZoom ,
     latestZoomFrozen,
     leafletElement,
     maxZoomUpdate,
     moveEnd,
     overlayChanged,
     setOldZoom,
     toogleSidebar,
     zoom,
    } = this.props;

    const { dd_latitude: focusedAirportLatitude,
            dd_longitude: focusedAirportLongitude } = focusedAirport;

    const mapIconsAttribution = "<a href='https://mapicons.mapsmarker.com'>Maps Icons Collection </a>";
    const openStreetMapAttribution = `&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors | ${mapIconsAttribution}`;
    const thunderforestAttribution = `Maps © <a href="http://www.thunderforest.com">Thunderforest</a>, Data © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap contributors</a> | ${mapIconsAttribution}`;

    return (
        <Map 
          center={center}
          zoom={zoom} 
          animate={animate}
          ref={"map"}
          onMoveend={(event)=> {
            if (focusedAirport.airport_id) {
              if (!leafletElement.getBounds().contains([focusedAirportLatitude, focusedAirportLongitude])) {
                toogleSidebar(false);
              }
            }
            if (!airlineIsFocused && (zoom > maxZoomUpdate)) {
              moveEnd(event);
            }
          }}
          onZoomend={(event)=>{
            const actualZoom = event.target.getZoom();
            getZoom(actualZoom);
            latestZoomFrozen ? freezeOldZoom(false): setOldZoom(actualZoom) ;

          }}
          zoomControl={false}
          onOverlayremove={overlayChanged}
          onOverlayadd={overlayChanged}
          style={{height: "100vh"}}>
          
            <LayersControl position="topright" >
              <LayersControl.BaseLayer name="OpenStreetMap.BlackAndWhite" checked>
                <TileLayer
                  attribution={openStreetMapAttribution}
                  url="http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="OpenStreetMap.Mapnik">
                <TileLayer
                  attribution={openStreetMapAttribution}
                  url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                />
              </LayersControl.BaseLayer>
              <LayersControl.BaseLayer name="Neighbourhood">
                <TileLayer
                  attribution={thunderforestAttribution}
                  url="https://{s}.tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png?apikey=b0637c42d7c84d379d2b26c811a4364a"
                />
              </LayersControl.BaseLayer>
              <LayersControl.Overlay name="Airports" checked >
                <Airports />
              </LayersControl.Overlay>
              <LayersControl.Overlay name="Airfields" checked>
                <Airfields />
              </LayersControl.Overlay>
            </LayersControl>
            <Sidebar />
            <Notifs 
              CustomComponent={TopBox} 
              componentClassName={"top-box-notification"} 
            />

            <ZoomControl position="topright" />
          </Map>
      );
  }
}

map.propTypes = {
  airlineIsFocused: React.PropTypes.bool,
  animate: React.PropTypes.bool.isRequired,
  center: React.PropTypes.object.isRequired,
  clearNotif: React.PropTypes.func,
  dispatchCenter: React.PropTypes.func,
  dispatchGetAirports: React.PropTypes.func,
  dispatchLeafletElement: React.PropTypes.func,
  dispatchNotification: React.PropTypes.func,
  focusedAirport: React.PropTypes.object,
  freezeOldZoom: React.PropTypes.func,
  getZoom: React.PropTypes.func,
  latestZoomFrozen: React.PropTypes.bool,
  leafletElement: React.PropTypes.object,
  maxZoomUpdate: React.PropTypes.number,
  moveEnd: React.PropTypes.func,
  notifs: React.PropTypes.array,
  overlayChanged: React.PropTypes.func,
  setOldZoom: React.PropTypes.func,
  toogleSidebar: React.PropTypes.func,
  zoom: React.PropTypes.number.isRequired

};


const mapStateToProps = (state) =>{
  return {
    airlineIsFocused: state.airlineIsFocused,
    animate: state.animate,
    center: state.center,
    focusedAirport: state.focusedAirport,
    latestZoomFrozen: state.latestZoomFrozen,
    leafletElement: state.leafletElement,
    maxZoomUpdate: state.maxZoomUpdate,
    notifs: state.notifs,
    zoom: state.zoom
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    clearNotif: id =>dispatch(notifDismiss(id)),
    dispatchCenter: (lat, lng) => dispatch(center({lat, lng})),
    dispatchGetAirports: (northEast, southWest) => dispatch(getAirports(northEast,southWest)),
    dispatchLeafletElement: leafletElement => dispatch(defineLeafletElement(leafletElement)),
    dispatchNotification: options => dispatch(notifSend(options)),
    freezeOldZoom: freeze => dispatch(setFreezeLatestZoomStatus(freeze)),
    getZoom: actualZoom => dispatch(zoom(actualZoom)),
    moveEnd: event => {
      const northEast = event.target.getBounds().getNorthEast();
      const southWest = event.target.getBounds().getSouthWest();
      dispatch(getAirports(northEast, southWest));
      // in case we need the store center updated when we move the map, uncomment the following line.
      // dispatch(center({lat: event.target.getCenter().lat, lng: event.target.getCenter().lng}));
    },
    overlayChanged: layer => dispatch(updateOverlay(layer)),
    setOldZoom: actualZoom => dispatch(updateLatestZoom(actualZoom)),
    toogleSidebar: (visible) => dispatch(changeSidebarVisibility(visible))
  };
};

const mapConnected = connect(mapStateToProps, mapDispatchToProps)(map);

export default mapConnected;
