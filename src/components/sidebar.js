import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import { changeSidebarVisibility } from "../actions/actions";
import classnames from "classnames";
import Airlines from "./airlines.js";


const logoStyle = {
  "textAlign": "right"
};
const identifyStyle = {
  "textAlign": "left"  
};
class sidebar extends React.Component {
  componentDidUpdate() {
    let nodeSidebar = ReactDOM.findDOMNode (this.refs.sidebar);
    nodeSidebar.scrollTop = 0;
  }

  disableMapEvents (mapElement){
    // Bermi: I am triggering this with mouseover to avoid when the sidebar 
    // fills the screen no mouseenter event triggers.
    // if it is ok, I'll delete the onmouseenter event.
    if (mapElement.scrollWheelZoom.enabled()) {
      mapElement.dragging.disable();
      mapElement.touchZoom.disable();
      mapElement.doubleClickZoom.disable();
      mapElement.scrollWheelZoom.disable();
      mapElement.boxZoom.disable();
      mapElement.keyboard.disable();
      if (mapElement.tap) mapElement.tap.disable();
    }
  } 

  enableMapEvents (mapElement){
    mapElement.dragging.enable();
    mapElement.touchZoom.enable();
    mapElement.doubleClickZoom.enable();
    mapElement.scrollWheelZoom.enable();
    mapElement.boxZoom.enable();
    mapElement.keyboard.enable();
    if (mapElement.tap) mapElement.tap.enable();
  }

  render (){
    const { toogleView, runways, sidebarVisible, focusedAirport, leafletElement } = this.props;
    const { airport_id , name , iata, icao, latitude, longitude } = focusedAirport;
    const sidebarClasses = classnames("leaflet-control-container", "leaflet-sidebar", "left" , {
      visible: sidebarVisible
    });

    return (
        <div 
          id={airport_id} 
          className={sidebarClasses} 
          onMouseEnter={()=> this.disableMapEvents(leafletElement)}
          onMouseLeave={()=> this.enableMapEvents(leafletElement)}
          onMouseOver={()=> this.disableMapEvents(leafletElement)}
          
          >
          
          <div id="sidebar" className="leaflet-control" ref={"sidebar"}>
            <h1>{name}</h1>
            <h3>Runways</h3>
            <table className="table table-striped ">
              <tbody>
                <tr>
                  <th>Direction</th>
                  <th>ft</th>
                  <th>m</th>
                  <th>Surface</th>
                </tr>

                {runways.map((runway) => {
                  const { direction, ft, m, surface} = runway;
                  return (

                       <tr key={airport_id + direction}>
                        <td>{direction}</td>
                        <td>{ft}</td> 
                        <td>{m}</td>
                        <td>{surface}</td>
                      </tr>
                      
                    );
                })}
              </tbody>
            </table>
            <h3>Data:</h3>
            <table style={{width:"100%", textAlign: "center"}}>
            <tbody>
              <tr>
                <td style={logoStyle}><img height="25px" src={require("../images/iata-logo.png")} alt="IATA"/></td>
                <td style={identifyStyle}>{iata}</td>
                <td style={logoStyle}><img height="25px" src={require("../images/icao-logo.png")} alt="ICAO"/></td>
                <td style={identifyStyle}>{icao}</td>
              </tr>
            </tbody>
            </table>
            <table style={{width:"100%", textAlign: "center"}}>
              <tbody >
                <tr>
                  <td>{latitude}</td>
                  <td>{longitude}</td>
                </tr>
              </tbody>
            </table>
            
            <Airlines />
          </div>
          <a onClick={() => toogleView()} className="close">×</a>
      </div> );
  }
}

sidebar.propTypes = {
  airport: React.PropTypes.object,
  children: React.PropTypes.array,
  focusedAirport: React.PropTypes.object,
  leafletElement: React.PropTypes.object,
  mapElement: React.PropTypes.object,
  runways: React.PropTypes.array,
  sidebarVisible: React.PropTypes.bool,
  toogleView: React.PropTypes.func
};

const mapStateToProps = state => {
  return {
    airlines: state.airlines,
    focusedAirport: state.focusedAirport,
    leafletElement: state.leafletElement,
    runways: state.runways,
    sidebarVisible: state.sidebarVisible
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toogleView: () => dispatch(changeSidebarVisibility())
  };
};

const sidebarConnected = connect(mapStateToProps, mapDispatchToProps)(sidebar);

export default sidebarConnected;