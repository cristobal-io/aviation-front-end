import React from "react";
import ReactDOM from "react-dom";

// importing styles
import "./styles/styles.scss";

// The following line renders the react-leaflet component.
import Map from "./components/map";

// store
import { initialState } from "./store/initialState";
import configureStore from "./store/configureStore"; //eslint-disable-line import/default

// Creating the store including dev tools and thunk
const store = configureStore(initialState);

/**
 *
 * 
 * doing some debugging, this lines have to be removed 
 * ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
 *
 * 
 */
// store.subscribe(()=>{
//   const actualStore = store.getState();
//   console.log("subscribed store.getState.latestZoomFrozen", actualStore.latestZoomFrozen);
//   console.log("subscribed store.getState.zoom", actualStore.zoom);
//   console.log("subscribed store.getState.oldZoom", actualStore.oldZoom);
// });
/**
 *
 * ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
 * this lines had to be removed
 *
 * 
 */

import { Provider } from "react-redux";

const app = (
  <Provider store={store}>
    <Map />
  </Provider>
  );

ReactDOM.render(app, document.getElementById("map-container"));


