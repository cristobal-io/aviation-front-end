const maxZoomUpdate = (state = 0, action) => {


  switch (action.type) {
  case "MAX_ZOOM_UPDATE":
    // console.log("maxZoomUpdate reducer called with state: ", state, "and action:", action);
    return action.maxZoomUpdate;
  default:
    return state;
  }
};

export default maxZoomUpdate;
