const focusedAirport = (state = {}, action) => {
  // console.log("showSidebar reducer called with state: ", state, "and action:", action);


  switch (action.type) {
  case "UPDATE_FOCUSED_AIRPORT":

    return action.airport;
  default:
    return state;
  }
};

export default focusedAirport;
