const getAirports = (state = [], action) => {

  // console.log("getAirports reducer called with state: ", state, "and action:", action);

  switch (action.type) {
  case "GET_AIRPORTS":
    return JSON.parse(action.airports);
  case "GET_DESTINATIONS":
    // console.log("getDestinations inside getAirportsReducer.js reducer called with state: ", state, "and action:", action);
  // console.log(JSON.parse(action.airlineDestinations));
    return action.airlineDestinations;

  default:
    return state;
  }
};

export default getAirports;
