const airlineFocused = (state = false, action) => {
  switch (action.type) {
    case "AIRLINE_IS_FOCUSED":
      return !state;
    default:
      return state;
  }
};

export default airlineFocused;