const oldFocusedAirline = (state = {}, action) => {


  switch (action.type) {
  case "UPDATE_OLD_FOCUSED_AIRLINE":
  // console.log("oldFocusedAirline reducer called with state: ", state, "and action:", action);

    return action.airline;
  default:
    return state;
  }
};

export default oldFocusedAirline;
