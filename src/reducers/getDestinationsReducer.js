

const getDestinations = (state = [], action) => {

  switch (action.type) {
  case "GET_DESTINATIONS":
    // console.log("getDestinations reducer called with state: ", state, "and action:", action);
    // console.log(JSON.parse(action.airlineDestinations));
    return action.airlineDestinations.map((destination) => {
      const lat = destination.dd_latitude,
      lng = destination.dd_longitude;
      if (lat === 0 && lng === 0) {
        // check made to avoid airports without coordinates.
        return;
      }
      return [lat, lng];
    });
  default:
    return state;
  }
};

export default getDestinations;
