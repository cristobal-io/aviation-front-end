const animate = (state = "", action) => {

  // console.log("animate reducer called with state: ", state, "and action:", action);

  switch (action.type) {
    case "TOGGLE_ANIMATE":
      // console.log("animate called with state", state, "and action ", action);
      return action.animate;
    default:
      return state;
  }
};

export default animate;