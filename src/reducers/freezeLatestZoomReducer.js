const latestZoomFrozen = (state = false, action) =>{
  switch (action.type) {
    case "FREEZE_LATEST_ZOOM":
      // console.log("FREEZE_LATEST_ZOOM reducer called with state:", state, "and action", action);
      return action.latestZoomFrozen;
    default :
      return state;
  }
};

export default latestZoomFrozen;