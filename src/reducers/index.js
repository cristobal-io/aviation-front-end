// Set up your root reducer here...
import { combineReducers } from "redux";

import center from "./centerReducer";
import animate from "./animateReducer";
import zoom from "./zoomReducer";
import airports from "./getAirportsReducer";
import focusedAirport from "./focusedAirportReducer";
import airlines from "./getAirlinesReducer";
import runways from "./getRunwaysReducer";
import sidebarVisible from "./sidebarVisibleReducer";
import leafletElement from "./leafletElementReducer";
import focusedAirline from "./focusedAirlineReducer";
import airlineDestinations from "./getDestinationsReducer";
import oldZoom from "./oldZoomReducer";
import airlineIsFocused from "./airlineIsFocusedReducer";
import maxZoomUpdate from "./maxZoomUpdateReducer";
import oldFocusedAirline from "./oldFocusedAirlineReducer";
import lastDestinationsCount from "./lastDestinationsCountReducer";
import notificationsClosed from "./notificationsClosedReducer";
import overlay from "./overlayReducer";
import latestZoomFrozen from "./freezeLatestZoomReducer";

import { reducer as notifReducer } from "redux-notifications";

export default combineReducers ({
  notifs: notifReducer,
  center,
  animate,
  zoom,
  oldZoom,
  leafletElement,
  airports,
  focusedAirport,
  focusedAirline,
  latestZoomFrozen,
  airlines,
  runways,
  sidebarVisible,
  airlineDestinations,
  airlineIsFocused,
  maxZoomUpdate,
  oldFocusedAirline,
  lastDestinationsCount,
  notificationsClosed,
  overlay
});