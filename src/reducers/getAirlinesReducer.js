const getAirlines = (state = {}, action) => {
  // console.log("getAirlines reducer called with state: ", state, "and action:", action);
  let airlinesObj;
  switch (action.type) {
  case "GET_AIRLINES":
    airlinesObj = action.airlines.reduce((accumulator, airline) => {
      accumulator[airline.airline_id] = airline;
      return accumulator;
    }, {});
    return airlinesObj;
  case "GET_AIRLINE":
    if (action.airline) {
      return {...state, [action.airline.airline_id]:action.airline};
    } else {
      return state;
    }

  default:
    return state;
  }
};

export default getAirlines;
