const sidebarVisible = (state = false, action) => {

  switch (action.type) {
  case "SIDEBAR_VISIBILITY":
  // console.log("sidebarVisible reducer called with state: ", state, "and action:", action);
    if (action.visible !== undefined) {
      return action.visible;
    } else {
      return !state;
    }
  default:
    return state;
  }
};

export default sidebarVisible;
