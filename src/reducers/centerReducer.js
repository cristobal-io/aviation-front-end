const center = (state = {}, action) => {


  switch (action.type) {
  case "CENTER":
    // console.log("center reducer called with state: ", state, "and action:", action);
    return action.center;
  default:
    return state;
  }
};

export default center;
