const oldZoom = (state=0, action) => {
  switch (action.type) {
  case "LATEST_ZOOM":
    // console.log("oldZoom reducer called with state: ", state, "and action:", action);
    return action.oldZoom;
  default:
    return state;
  }
};

export default oldZoom;