const getRunways = (state = [], action) => {
  // console.log("getRunways reducer called with state: ", state, "and action:", action);


  switch (action.type) {
  case "GET_RUNWAYS":
    return JSON.parse(action.runways);
  default:
    return state;
  }
};

export default getRunways;
