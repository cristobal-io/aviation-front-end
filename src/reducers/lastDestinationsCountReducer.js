const lastDestinationsCount = (state=0, action) => {
  switch (action.type) {
  case "UPDATE_OLD_DESTINATIONS_COUNT":
    // console.log("lastDestinationsCount reducer called with state: ", state, "and action:", action);
    return action.lastDestinationsCount;
  default:
    return state;
  }
};

export default lastDestinationsCount;