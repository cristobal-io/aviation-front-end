const overlay = (state = [], action) => {


  switch (action.type) {
    case "OVERLAY_CHANGED":
      // console.log("overlay reducer called with state: ", state, "and action:", action);
      if (action.overlay.type === "overlayremove") {
        return state.filter(name => name !== action.overlay.name);
      } else if (action.overlay.type === "overlayadd") {
        return state.concat(action.overlay.name);
      }
      break;
    default:
      return state;
  }
};

export default overlay;
