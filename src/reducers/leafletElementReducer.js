const leafletElement = (state = {}, action) => {


  switch (action.type) {
  case "LEAFLET_ELEMENT":
    // console.log("leafletElement reducer called with state: ", state, "and action:", action);
    return action.leafletElement;
  default:
    return state;
  }
};

export default leafletElement;
