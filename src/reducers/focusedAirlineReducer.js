const focusedAirline = (state = {}, action) => {


  switch (action.type) {
  case "UPDATE_FOCUSED_AIRLINE":
  // console.log("focusedAirline reducer called with state: ", state, "and action:", action);

    return action.airline;
  default:
    return state;
  }
};

export default focusedAirline;
