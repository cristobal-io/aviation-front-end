/*
Aviation API settings
 */
const AVIATION_API_HOSTNAME = process.env.AVIATION_API_HOSTNAME || "localhost";
const AVIATION_API_PORT = +(process.env.AVIATION_API_PORT || 8081);

/*
Map status
 */
const TOGGLE_ANIMATE = "TOGGLE_ANIMATE";

export const toggleAnimate = state => ({
  type: TOGGLE_ANIMATE,
  animate: !state.animate
});


const CENTER = "CENTER";

export const center = coords => ({
  type: CENTER,
  center: coords
});

const ZOOM = "ZOOM";

export const zoom = zoom => ({
  type: ZOOM,
  zoom
});

const LATEST_ZOOM = "LATEST_ZOOM";

export const updateLatestZoom = zoom => ({
  type: LATEST_ZOOM,
  oldZoom: zoom
});

const FREEZE_LATEST_ZOOM = "FREEZE_LATEST_ZOOM";

export const setFreezeLatestZoomStatus = freeze => ({
  type: FREEZE_LATEST_ZOOM,
  latestZoomFrozen: freeze
});

const LEAFLET_ELEMENT = "LEAFLET_ELEMENT";

export const defineLeafletElement = leafletElement => ({
  type: LEAFLET_ELEMENT,
  leafletElement
});

const OVERLAY_CHANGED = "OVERLAY_CHANGED";

export const updateOverlay = layer => ({
  type: OVERLAY_CHANGED,
  overlay: layer
});

/*
sidebar and top
 */
const SIDEBAR_VISIBILITY = "SIDEBAR_VISIBILITY";

export const changeSidebarVisibility = (visible) => ({
  type: SIDEBAR_VISIBILITY,
  visible
});


const UPDATE_NOTIFICATION_CLOSED = "UPDATE_NOTIFICATION_CLOSED";

export const updateNotificationClosed = id =>({
  type:UPDATE_NOTIFICATION_CLOSED,
  id
});

/*
Airports
 */

const UPDATE_FOCUSED_AIRPORT = "UPDATE_FOCUSED_AIRPORT";
export const updateFocusedAirport = (airport) => ({
  type: UPDATE_FOCUSED_AIRPORT,
  airport
});

const GET_AIRPORTS = "GET_AIRPORTS";
import http from "http";

export const getAirports = (northEast, southWest) => {
  return function (dispatch) {
    let options = {
      hostname: AVIATION_API_HOSTNAME,
      port: AVIATION_API_PORT,
      path: `/airports/box/?lat1=${northEast.lat}&long1=${northEast.lng}&lat2=${southWest.lat}&long2=${southWest.lng}`
    };
    connectApi(options, (err, data)=>{
      if (err) {
        throw err;
      }
      dispatch(({
        type: GET_AIRPORTS,
        airports: data
      }));

    });
  };
};


const GET_RUNWAYS = "GET_RUNWAYS";

export const getRunways = airportName => {
  return function (dispatch) {
    let options = {
      hostname: AVIATION_API_HOSTNAME,
      port: AVIATION_API_PORT,
      path: `/airports/runways/?name=${airportName}`
    };
    connectApi(options, (err, data) => {
      if (err) {
        throw err;
      }
      dispatch(({
        type: GET_RUNWAYS,
        runways: data
      }));
    });
  };
};

/*
airlines
 */
const GET_AIRLINES = "GET_AIRLINES";

export const getAirlines = airport_id => {
  return function (dispatch) {
    let airlinesByAirportOptions = {
      hostname: AVIATION_API_HOSTNAME,
      port: AVIATION_API_PORT,
      path: `/airport/airlines/${airport_id}`
    };

    connectApi(airlinesByAirportOptions, (err, data) => {
      if (err) {
        throw err;
      }

      dispatch(({
        type: GET_AIRLINES,
        airlines: JSON.parse(data)
      }));
    });
  };
};

const GET_AIRLINE = "GET_AIRLINE";
export const getAirline = (airlineId) => {
  let airlineDataOptions = {
    hostname: AVIATION_API_HOSTNAME,
    port: AVIATION_API_PORT,
    path: `/airlines/?airline_id=${airlineId}`
  };
  return (dispatch) => {

    connectApi(airlineDataOptions, (err, airlineData) => {
      airlineData = JSON.parse(airlineData);
      dispatch(({
        type: GET_AIRLINE,
        airline: airlineData[0]
      }));
    });
  };
};

const UPDATE_FOCUSED_AIRLINE = "UPDATE_FOCUSED_AIRLINE";
export const updateFocusedAirline = (airline) => ({
  type: UPDATE_FOCUSED_AIRLINE,
  airline
});

const UPDATE_OLD_FOCUSED_AIRLINE = "UPDATE_OLD_FOCUSED_AIRLINE";
export const updateOldFocusedAirline = (airline) => ({
  type: UPDATE_OLD_FOCUSED_AIRLINE,
  airline
});

const UPDATE_OLD_DESTINATIONS_COUNT = "UPDATE_OLD_DESTINATIONS_COUNT";
export const updateOldDestinationsCount = (lastDestinationsCount) => ({
  type: UPDATE_OLD_DESTINATIONS_COUNT,
  lastDestinationsCount
});

const AIRLINE_IS_FOCUSED = "AIRLINE_IS_FOCUSED";
export const airlineIsFocused = airline => ({
  type: AIRLINE_IS_FOCUSED,
  airlineFocused: airline
});

const GET_DESTINATIONS = "GET_DESTINATIONS";

export const getDestinations = airlineId => {
  return function (dispatch) {
    let airlineDestinationsOptions = {
      hostname: AVIATION_API_HOSTNAME,
      port: AVIATION_API_PORT,
      path: `/airlines/destinations/${airlineId}`
    };

    connectApi(airlineDestinationsOptions, (err, data) => {
      if (err) {
        throw err;
      }

      dispatch(({
        type: GET_DESTINATIONS,
        airlineDestinations: JSON.parse(data)
      }));
    });
  };
};

// Bermi: I know that best practice is to return with the callback the error,
// but wouldn't be in this specific case a good chance to throw the error directly?
const connectApi = (options, callback) => {
  let str = "";
  http.get(options, (res) => {
    res.on("data", (chunk) => {
      str+=chunk;
    });
    res.on("end", ()=>{
      callback(null, str);
    });
  }).on("error", (e) => {
    callback(e);
  });
};
