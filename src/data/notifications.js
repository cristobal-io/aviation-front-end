export const initialInfo = {
  message: "Move the map to see the airports, click at the desired airport to see more information.",
  dismissAfter: 5000,
  id: "initialInfo"
};

export const selectOtherAirportInfo = {
  message: "If you want to see another airport information, select it, you won't loose the airline destinations.",
  dismissAfter: 12000,
  id: "selectOtherAirportInfo"
};

export const selectOtherAirlineInfo = {
  message: "Select another airline to see its destinations",
  dismissAfter: 12000,
  id: "selectOtherAirlineInfo"
};

export const viewDestinationsInfo = {
  message: "Select an airline to view all the destinations of the airline.",
  dismissAfter: 8000,
  id: "viewDestinationsInfo"
};

export const unselectInfo = {
  message: "Quit the selection and you'll center at the sidebar airport with the previous zoom.",
  dismissAfter: 12000,
  id: "unselectInfo"
};

export const zoomWarning = {
  message: "Zoom is too big, please reduce zoom to update airports location.",
  kind: "warning",
  dismissAfter: null,
  id: "bigZoom"
};