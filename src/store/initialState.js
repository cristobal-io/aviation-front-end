/*eslint-disable no-unused-vars*/

const stanford = {
  "lat": 37.42688834526725,
  "lng": -122.16779708862305
};

const sfo = {
  "lat": 37.61888888888889,
  "lng": -122.375
};

export const initialState = {
  "animate": true,
  "center": sfo,
  "zoom": 10,
  "maxZoomUpdate": 5,
  "overlay": ["Airports", "Airfields"]
};

