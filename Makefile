SHELL = /bin/bash
MAKEFLAGS += --no-print-directory --silent
export PATH := ./node_modules/.bin:$(PATH):./bin

# linting directories
LINT_DIR = $(wildcard src/**/*.js src/*.js)


default: setup start

setup:
	npm install

start:
	make aviation-api-server & npm run start || true
	echo "killing the server for aviation-api"
	pkill -f "node node_modules/aviation-api/src/index.js"

.PHONY: dist
dist:
	npm run clean-dist
	NODE_ENV=production npm run dist

lint:
	echo "eslint started..."
	npm run lint
	echo "eslint finished."

lint-fix:
	echo "eslint started..."
	eslint --fix $(LINT_DIR)
	echo "eslint finished."

.PHONY: test
test: lint
	npm run test

aviation-api-server:
	DEBUG=aviation* \
	AVIATION_CORS_ORIGIN="http://localhost:3000" \
	node node_modules/aviation-api/src/index.js

test-coverage-report:
	echo "Generating coverage report, please stand by"
	test -d node_modules/nyc/ || npm install nyc
	nyc mocha && nyc report --reporter=html
	open coverage/index.html

clean:
	npm run clean-dist

# Continuous Integration Test Runner
ci: lint test
	echo "1. 'make clean'"
	echo "2. Make sure 'git status' is clean."
	echo "3. 'git checkout -b (release-x.x.x || hotfix-x.x.x) master"
	echo "4. 'git merge dev --no-ff --log'"
	echo "5. 'Make release'"

release: lint
	echo "1. 'git checkout master'"
	echo "2. 'git merge (release-x.x.x || hotfix-x.x.x) --no-ff --log'"
	echo "3. 'release-it'"
	echo "4. 'git checkout dev'"
	echo "5. 'git merge (release-x.x.x || hotfix-x.x.x) --no-ff --log'"
	echo "6. 'git branch -d (release-x.x.x || hotfix-x.x.x)'"

docker-build:
	docker build -t aviation-front-end .

docker-run:
	docker run -it --rm --name aviation-front-end \
	-p 127.0.0.1:8080:80 aviation-front-end