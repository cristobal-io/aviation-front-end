# aviation-front-end

Exposure of the aviation modules previously released.

This package is created using the starter kit [react-slingshot](https://github.com/coryhouse/react-slingshot) and Leaflet.

The project has been developed using the latest technologies, React, Redux and written in ES6.

### Example

<!-- Access the [live demo](http://aviation.cristobal.io) -->

![Demo Map](./demo_map.gif "Demo Map")


## Usage

To use it, you need to have the database provided by [aviation-pg](https://github.com/cristobal-io/aviation-pg) please check it and follow the instructions to have up and running the Postgresql DB.

Clone the package and enter the directory:

```bash
git clone https://github.com/cristobal-io/aviation-front-end.git && cd aviation-front-end
```

Run the command:

```bash
make
```

This will do for you the following steps:

1. `npm install`
2. apply patch to Access-Control-Allow-Origin on aviation-api server.
3. start the application and open it on your default browser.

To start the application once you've done all the previous steps, you only need to use the command `make start`.

### Contributions:

If you want to contribute, create your branch and place a PR or open an issue.

## License

`aviation-front-end` is MIT licensed.

See [LICENSE](./LICENSE) for details.